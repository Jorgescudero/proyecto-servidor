<!DOCTYPE html>
<html>
<head>
	<title>Ejercicio 11</title>
	<style type="text/css">
		.error{
			color: red;
		}
	</style>
</head>
<body>
	<?php
	function test_input($data) {
 		$data = trim($data);
 		$data = stripslashes($data);
 		return $data;
	}

	function comprobar_nombre($name) {
 		if (empty($_POST["name"])) {
 		$nameErr = "Se requiere nombre";
 	} else {
		$name = test_input($_POST["name"]);
 		// check if name only contains letters and whitespace
 		if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
 		$nameErr = "Solo letras y espacios en blanco permitidos";
 		}else{
 			$nameErr="Nombre correcto";
 		}
 	}
 	return $nameErr;
	}

	function comprobar_email($email) {
 		$emailErr="Email correcto";
	if (empty($email)) {
		 $emailErr = "Se requiere Email";
 		} else {
 			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
 		$emailErr = "Formato de Email invalido";
 		}
 	}
 	return $emailErr;
	}

	function comprobar_url($url) {
 		$urlErr="URL correcto";
	if (empty($url)) {
		 $urlErr = "Se requiere URL";
 		} else {
 			if (!filter_var($url, FILTER_VALIDATE_URL)) {
 		$urlErr = "Formato de URL invalido";
 		}else{
 			$urlErr = "Formato correcto";
 		}
 	}
 	return $urlErr;
	}

	function comprobar_sexo($sexo) {
 		$sexoErr="Sexo perfectamente introducido";
	if (empty($sexo)) {
		 $sexoErr = "Se requiere sexo";
 		} 
 	
 	return $sexoErr;
	}

	if (!isset($_POST["Aceptar"])) {
		$url = "";
		$urlErr = "Se requiere URL";
		$email = "";
		$emailErr = "Se requiere Email";
		$name = "";
		$nameErr = "Se requiere nombre";
		$comment = "";
		$sexoErr = "Se requiere sexo";
		$sexo = " ";


	}else{
		$url = $_POST["url"];
		$urlErr=comprobar_url($url);
		$email = $_POST["email"];
		$emailErr=comprobar_email($email);
		$name = $_POST["name"];
		$nameErr = comprobar_nombre($name);
		$comment = $_POST["comment"];
		if(isset($_POST["sexo"])){
		$sexo = $_POST["sexo"];
		$sexoErr = comprobar_sexo($sexo);
		}else{
			$sexoErr = "Se requiere sexo";
			$sexo = "";
		}
	}
	?>

	<form action="" method="POST">
		Name: <input type="text" name="name" value="<?php if(isset($_POST["Aceptar"])){
			echo $name;
		}else{
			echo "";}?>"
		>
		<span class="error">*<?php echo $nameErr;?></span><br><br>
		Email: <input type="text" name="email" value="<?php if(isset($_POST["Aceptar"])){
			echo $email;
		}else{
			echo "";}?>"
		>
		<span class="error">*<?php echo $emailErr;?></span><br><br>
		URL: <input type="text" name="url" value="<?php if(isset($_POST["Aceptar"])){
			echo $url;
		}else{
			echo "";}?>"
		>
		<span class="error">*<?php echo $urlErr;?></span><br><br>
		Comments:<textarea type="text" name="comment" value="<?php if(isset($_POST["Aceptar"])){
			echo $comment;
		}?>"></textarea> <br><br>
		<input type="radio" name="sexo" <?php if (isset($sexo) && $sexo=="mujer") echo "checked";?> value="mujer"> Mujer 
		<input type="radio" name="sexo" <?php if (isset($sexo) && $sexo=="hombre") echo "checked";?> value="hombre"> Hombre 
		<span class="error">* <?php echo $sexoErr;?></span><br><br>
		<input type="submit" name="Aceptar"/>
		<br><br>
	</form>

	<?php
	if(isset($_POST["Aceptar"])){
	echo "Datos introducidos: <br>";
	if(!empty($name)){echo "Nombre: $name <br>";}
	if(!empty($email)){echo "Email: $email <br>";}
	if(!empty($url)){echo "URL: $url <br>";}
	if(!empty($comment)){echo "Comments: $comment <br>";}
	if(!empty($sexo)){echo "Sexo: $sexo <br>";}
	}
	



	?>
</body>
</html>