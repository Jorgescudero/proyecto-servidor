<?php

session_start();
$_SESSION['servidor']='localhost';
$_SESSION['basedatos']='ventas';
$_SESSION['usu1']='Administrador';
$_SESSION['pass1']='Administrador';
$_SESSION['usu2']='Consultor';
$_SESSION['pass2']='Consultor';

?>

<!DOCTYPE html>
<html>
<head>
	<link href="https://fonts.googleapis.com/css?family=Bangers|Nunito&display=swap" rel="stylesheet"> 
	<style type="text/css">
		html,body{
			background-image: linear-gradient(#e66465, #9198e5);
			width: 100%;
			height: 100%;
			font-family: 'Nunito', sans-serif;
		}
		header{
			font-family: 'Bangers', cursive;
			font-size: 30px;
			margin-left: 25%;
			width: 650px;
			text-align: center;
			background-color: rgba(120, 120, 120, 0.2);
		}

		form{
			width: 300px;
			margin-left: 37%;
			margin-top: 7%;
			text-align: center;
			background-color: rgba(120, 120, 120, 0.3);
		}

		button{ 
			width: 250px;
			height: 50px;
			font-size: 17px;
			background-color: rgba(120, 120, 120, 0.3);
		}

		h1{
			font-size: 30px;
			margin-left: 25%;
			width: 650px;
			text-align: center;
			background-color: rgba(120, 120, 120, 0.2);
		}

	</style>
</head>
<body>
	<header><h3>BIENVENID@ A TECNOMUNDO</h3></header>
	<?php

	if ($_SESSION['rol']=='consultor') {
	
	?>

	<title>Inicio <?php echo $_SESSION['rol']; ?></title>
	<h1>Bienvenido/a <?php echo $_SESSION['user']; ?>, se ha identificado como <?php echo $_SESSION['rol'] ?></h1>
	<form action="" method="POST">
		<fieldset>
			<legend>Menú de opciones</legend>
			<p><button type="submit" name="comprar">Comprar</button></p>
			<p><button type="submit" name="visualizarcompra">Visualizar compras</button></p>
			<p><button type="submit" name="cerrar">Cerrar sesion</button></p>
		</fieldset>
	</form>
	<?php
		if (isset($_POST['comprar'])) {

				header("Location:carrito.php");

			}

			if (isset($_POST['visualizarcompra'])) {

				header("Location:compras.php");

			}

			if (isset($_POST['cerrar'])) {

				header("Location:index.php");
				session_destroy();

			}

	} 
	if ($_SESSION['rol']=='administrador') {

	?>
	<title>Inicio <?php echo $_SESSION['rol']; ?></title>
	<h1>Bienvenido/a <?php echo $_SESSION['user']; ?>, se ha identificado como <?php echo $_SESSION['rol'] ?></h1>
	<form action="" method="POST">
		<fieldset>
			<legend>Menú de opciones</legend>
			<p><button type="submit" name="introducir">Introducir articulos</button></p>
			<p><button type="submit" name="visualizar">Visualizar Articulos</button></p>
			<p><button type="submit" name="cerrar">Cerrar sesion</button></p>
		</fieldset>
	</form>

	<?php


			if (isset($_POST['introducir'])) {

				header("Location:introducirarticulos.php");

			}

			if (isset($_POST['visualizar'])) {

				header("Location:visualizararticulos.php");

			}

			if (isset($_POST['cerrar'])) {

				header("Location:index.php");
				session_destroy();
				
			}
		}
	?>
</body>
</html>