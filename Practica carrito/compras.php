<!DOCTYPE html>
<html lang="es">
<head>
	<title>Carrito</title>
	<meta charset="utf-8">
	<link href="https://fonts.googleapis.com/css?family=Bangers|Nunito&display=swap" rel="stylesheet"> 
	<style type="text/css">
	html,body{
			background-image: linear-gradient(#e66465, #9198e5);
			width: 100%;
			height: 100%;
			font-family: 'Nunito', sans-serif;
		}
		header{
			font-family: 'Bangers', cursive;
			font-size: 30px;
			margin-left: 25%;
			width: 650px;
			text-align: center;
			background-color: rgba(120, 120, 120, 0.2);
		}

		.boton1{ 
			width: 150px;
			height: 50px;
			font-size: 17px;
			background-color: rgba(120, 120, 120, 0.3);
			margin-left: 37%;
		}
		.boton{
			width: 150px;
			height: 50px;
			font-size: 17px;
			background-color: rgba(120, 120, 120, 0.3);
		}

		.boton2{ 
			width: 200px;
			height: 50px;
			font-size: 17px;
			background-color: rgba(120, 120, 120, 0.3);
			margin-left: 32.6%;
		}
		.boton3{
			width: 200px;
			height: 50px;
			font-size: 17px;
			background-color: rgba(120, 120, 120, 0.3);
		}
		table{
			width: 600px;
			margin-left: 27%;
			margin-top: 30px;
			background-color: rgba(120, 120, 120, 0.5);
		}

		.fechas{
			width: 300px;
			margin-left: 37%;
			margin-top: 10%;
			text-align: center;
			background-color: rgba(120, 120, 120, 0.3);
		}
	</style>
</head>
<body>
	<header><h3>Desde aqui puede visualizar sus compras cuando usted quiera</h3></header>
	<div>
		<form action="" method="POST">
			<button type="submit" name="back" class="boton1">Volver al menú</button>
			<button type="submit" name="cerrarsesion" class="boton">Cerrar Sesión</button>
		</form>
	</div>
	<?php
		session_start();
		$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu2'], $_SESSION['pass2'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}
			?>
			<form action="" method="POST">
			<p><button type="submit" name="todas" class="boton2">Todas las compras</button>
			<button type="submit" name="buscar" class="boton3">Buscar compra</button></p>
			</form>

	<?php
		if (isset($_POST['todas'])) {
	?>
	
	<table border="1" style="text-align: center;">
    	<tr><th>Producto</th><th>Fecha</th><th>Unidades</th><th>Precio Unitario</th><th>Precio Total</th></tr>

  <?php
  	$usuario=$_SESSION['idusuario'];
  	$sql="SELECT descripcion,Fecha,Cantidad,compras.Precio from articulos,compras where '$usuario'=compras.idCliente AND compras.idProducto=articulos.id_articulo ORDER BY compras.Fecha";
  	$resultado= mysqli_query ($conexion, $sql);
	$filas=mysqli_num_rows($resultado);
	if ($filas>0) {
		while ($registro = mysqli_fetch_row($resultado)) {
			echo "<tr><td>".$registro[0]."</td><td>".$registro[1]."</td><td>".$registro[2]."</td><td>".$registro[3]."</td><td>".($registro[2]*$registro[3])."</td></tr>";
		}

	}else{
		echo "<tr><td colspan='5'>No ha hecho ninguna compra</td></tr>";
	}

	}


	if (isset($_POST['buscar'])) {
		?>
	<form name="altaForm" action="" method="POST" style="width: 300px;
			margin-left: 37%;
			margin-top: 20px;
			text-align: center;
			background-color: rgba(120, 120, 120, 0.3);">
		<p>Fecha 1: <input type="date" name="fecha1" id="fecha1" onblur="valf1()"></p>
		<p>Fecha 2: <input type="date" name="fecha2" id="fecha2" onblur="valf2()"></p>
		<button type="submit" name="comprobar">Comprobar</button>
	</form>
		<?php

		

	}

	if (isset($_POST['comprobar'])) {
		if ($_POST['fecha1']<$_POST['fecha2']) {
			$fecha1=$_POST['fecha1'] ." 00:00:00";
			$fecha2=$_POST['fecha2'] ." 23:59:59"; 

		echo '<table border="1" style="text-align: center;"><tr><th>Producto</th><th>Fecha</th><th>Unidades</th><th>Precio Unitario</th><th>Precio Total</th></tr>';
    	$usuario=$_SESSION['idusuario'];
  		$sql1="SELECT descripcion,Fecha,Cantidad,compras.Precio from articulos,compras where '$usuario'=compras.idCliente AND compras.idProducto=articulos.id_articulo AND ('$fecha1' <= compras.Fecha  AND '$fecha2' >= compras.Fecha) ORDER BY compras.Fecha";
  		$resultado1= mysqli_query ($conexion, $sql1);
		$filas1=mysqli_num_rows($resultado1);
		if ($filas1>0) {
		while ($registro1 = mysqli_fetch_row($resultado1)) {
			echo "<tr><td>".$registro1[0]."</td><td>".$registro1[1]."</td><td>".$registro1[2]."</td><td>".$registro1[3]."</td><td>".($registro1[2]*$registro1[3])."</td></tr>";
		}

	}else{
		echo "<tr><td colspan='5'>No ha hecho ninguna compra</td></tr>";
	}
		}else{
			$fecha2=$_POST['fecha2'] ." 00:00:00";
			$fecha1=$_POST['fecha1'] ." 23:59:59"; 

		echo '<table border="1" style="text-align: center;"><tr><th>Producto</th><th>Fecha</th><th>Unidades</th><th>Precio Unitario</th><th>Precio Total</th></tr>';
    	$usuario=$_SESSION['idusuario'];
  		$sql1="SELECT descripcion,Fecha,Cantidad,compras.Precio from articulos,compras where '$usuario'=compras.idCliente AND compras.idProducto=articulos.id_articulo AND ('$fecha2' <= compras.Fecha  AND '$fecha1' >= compras.Fecha) ORDER BY compras.Fecha";
  		$resultado1= mysqli_query ($conexion, $sql1);
		$filas1=mysqli_num_rows($resultado1);
		if ($filas1>0) {
		while ($registro1 = mysqli_fetch_row($resultado1)) {
			echo "<tr><td>".$registro1[0]."</td><td>".$registro1[1]."</td><td>".$registro1[2]."</td><td>".$registro1[3]."</td><td>".($registro1[2]*$registro1[3])."</td></tr>";
		}

	}else{
		echo "<tr><td colspan='5'>No ha hecho ninguna compra</td></tr>";
	}
		}
		
	}

	if (isset($_POST['back'])) {

		header("Location:inicio.php");

	}

	if (isset($_POST['cerrarsesion'])) {

		session_destroy();
			 
		header("Location:index.php");
	}
	mysqli_close($conexion);
	?>

	<script type="text/javascript">
		function valf1() {
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1;
			var yyyy = today.getFullYear();
			if (dd<10) {
				dd='0'+dd;
			}
			if (mm<10) {
				mm='0'+mm;
			}
			today = yyyy+"-"+mm+"-"+dd;

			var fecha = document.altaForm.fecha1.value;

			var cf=fecha.localeCompare(today);

			if (cf==1) {
				document.getElementById('fecha1').style.border="3px solid red";
				return false;
			}

			else {
				document.getElementById('fecha1').style.border="3px solid green";
				return true;
			}
		}

		function valf2() {
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1;
			var yyyy = today.getFullYear();
			if (dd<10) {
				dd='0'+dd;
			}
			if (mm<10) {
				mm='0'+mm;
			}
			today = yyyy+"-"+mm+"-"+dd;

			var fecha = document.altaForm.fecha2.value;

			var cf=fecha.localeCompare(today);

			if (cf==1) {
				document.getElementById('fecha2').style.border="3px solid red";
				return false;
			}

			else {
				document.getElementById('fecha2').style.border="3px solid green";
				return true;
			}
		}
	</script>
	
</body>
</html>