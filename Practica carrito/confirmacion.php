	<!DOCTYPE html>
<html lang="es">
<head>
	<title>Carrito</title>
	<meta charset="utf-8">
	<link href="https://fonts.googleapis.com/css?family=Bangers|Nunito&display=swap" rel="stylesheet">
	<style type="text/css">
	html,body{
			background-image: linear-gradient(#e66465, #9198e5);
			width: 100%;
			height: 100%;
			font-family: 'Nunito', sans-serif;
		}
		header{
			font-family: 'Bangers', cursive;
			font-size: 30px;
			margin-left: 25%;
			width: 650px;
			text-align: center;
			background-color: rgba(120, 120, 120, 0.2);
		}
		td{
			width: 200px;
			background-color: rgba(120, 120, 120, 0.3);
			border-radius: 15px;
		}

		table{
			margin-left: 25%;
			margin-top: 30px;
		}
		.boton1{ 
			width: 150px;
			height: 50px;
			font-size: 17px;
			background-color: rgba(120, 120, 120, 0.3);
			margin-left: 42%;
		}

		h4{
			text-align: center;
			font-size: 20px;
		}
	</style>
</head>
<body>
	<header><h3>Gracias por su compra,vuelva pronto</h3></header>
	
	<?php
	session_start();
		$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu2'], $_SESSION['pass2'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
	    	}
	    if (isset($preciototal)==false) {
				$preciototal=0;
			}
	?>

	<table>
    	<tr><th>Producto</th><th>Unidades</th><th>Precio</th></tr>
    	<?php
    	foreach ($_SESSION['cantidad'] as $key => $value) {
			if ($value>0) {
				$sql3="SELECT precio,id_articulo from articulos where '$key'=articulos.descripcion";
				
				$resultado3= mysqli_query ($conexion, $sql3);
				$filas3=mysqli_num_rows($resultado3);
				if ($filas3>0) {
				while ($registro3 = mysqli_fetch_row($resultado3)) {
					echo '<tr><td>'. $key .'</td><td>' .$value .'</td><td>'  .($value*$registro3[0]) .'</td><tr>';
					$preciototal+=($value*$registro3[0]);
					$date = date('Y-m-d H:i:s');
					$usuario=$_SESSION['idusuario'];
					$precio=$registro3[0];
					$idProducto=$registro3[1];
					$sql2="INSERT into compras (idCliente,idProducto,Fecha,Cantidad,Precio) values ($usuario,$idProducto,'$date',$value,$precio);";
					
					if (mysqli_multi_query($conexion, $sql2)) {
      					$aviso1 = "<h4>Ha realizado correctamente la compra</h4>";
    				} else {
      					$aviso1 = " <br>Error: " . $sql2 . "<br>" . mysqli_error($conexion);
    				}
				}
			}
			}

		}
    	echo "<td>Total: </td><td>".$preciototal."€</td></tr>";
   	 	?>
   	 </table>

   	 <?php echo $aviso1; 

	if (isset($_POST['cerrarsesion'])) {

		session_destroy();
			 
		header("Location:index.php");
	}
	mysqli_close($conexion);
	?> 
	<div>
		<form action="" method="POST">
			<button type="submit" name="cerrarsesion" class="boton1">Cerrar Sesión</button>
		</form>
	</div>
   	</body>
   	</html>