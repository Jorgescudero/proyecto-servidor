<!DOCTYPE html>
<html lang="es">
<head>
	<title>Ventas</title>
	<meta charset="utf-8">
	<link href="https://fonts.googleapis.com/css?family=Bangers|Nunito&display=swap" rel="stylesheet"> 
	<style type="text/css">
		html,body{
			background-image: linear-gradient(#e66465, #9198e5);
			width: 100%;
			height: 100%;
			font-family: 'Nunito', sans-serif;
		}
		header{
			font-family: 'Bangers', cursive;
			font-size: 30px;
			margin-left: 25%;
			width: 650px;
			text-align: center;
			background-color: rgba(120, 120, 120, 0.2);
		}
		.primero{
			width: 300px;
			margin-left: 37%;
			margin-top: 10%;
			text-align: center;
			background-color: rgba(120, 120, 120, 0.3);
		}

		.segundo{
			background-color: lightgrey;
			width: 300px;
			text-align: center;
			margin-left: 37%;
			background-color: rgba(120, 120, 120, 0.3);
		}

		.boton{ 
			width: 150px;
			height: 50px;
			font-size: 17px;
			background-color: rgba(120, 120, 120, 0.3);
		}

		.boton1{
			background-color: rgba(120, 120, 120, 0.7);
			font-size: 14px;
		}
		
	</style>
</head>
<body>
	<header><h3>BIENVENID@ A TECNOMUNDO</h3></header>
	<form action="" method="POST" class="primero">
			<p><button class="boton"  type="submit" name="iniciarsesion">Inicio de sesion</button><button class="boton" type="submit" name="registro">Registrarse</button></p>
		
	</form>
	<?php
		if (isset($_POST['iniciarsesion'])) {
	?>
	<form action="" method="POST" class="segundo">
			<legend>Inicio de sesión</legend>
			<p>Usuario: <br><input type="text" name="user" style="text-align: center;"></p>
			<p>Contraseña: <br><input type="password" name="pass" style="text-align: center;"></p>
			<p><input type="submit" name="enviar" value="Entrar" class="boton1"></p>
	</form>

	<?php
	}
	if (isset($_POST['registro'])) {
		?>

	<form action="" method="POST" class="segundo">
			<legend>Va a registrarse</legend>
			<p>Nombre: <input type="text" name="usernuevo"></p>
			<p>Contraseña: <input type="password" name="passnuevo"></p>
			<p>Repetir contraseña: <input type="password" name="passrepetida"></p>
			<p>Tipo <input type="radio" name="tipo" value="consultor" checked>Consultor</input><input type="radio" name="tipo" value="administrador" checked>Administrador</input></p>
			<p><input type="submit" name="registrar" value="Entrar" class="boton1"></p>
	</form>
	<?php
	}
	if (isset($_POST['enviar'])) {
		$conexion = mysqli_connect('localhost', 'Consultor', 'Consultor', 'ventas');
		if (mysqli_connect_errno()) {
		    printf("Conexión fallida %s\n", mysqli_connect_error());
		    exit();
		}
			$usuario=$_POST['user'];
			$password=$_POST['pass'];
			$passwordencryp= hash_hmac('sha512', $password, 'primeraweb');
			$sql= "SELECT rol,idusuario FROM usuarios WHERE usuario = '$usuario' AND password = '$passwordencryp'";
			$result = mysqli_query ($conexion, $sql);
				if(mysqli_num_rows($result) > 0) {
					while ($registro = mysqli_fetch_row($result)) {
		                $rol=$registro[0];
		                $usuarioid=$registro[1];
		        	}
		        	session_start();
		        	$_SESSION['idusuario']=$usuarioid;
			        $_SESSION['rol']="$rol";
			        $_SESSION['user']=$usuario;

			        header("Location:inicio.php");

			        exit();
			        }else{
					$mensajeaccesoincorrecto = "<p style='text-align:center'>El usuario o contraseña no son correctos, vuelva a introducirlos</p>";
		        	echo $mensajeaccesoincorrecto;
				}
		}

	if (isset($_POST['registrar'])){
		$conexion = mysqli_connect('localhost', 'Administrador', 'Administrador', 'ventas');
		if (mysqli_connect_errno()) {
		    printf("Conexión fallida %s\n", mysqli_connect_error());
		    exit();
		}
		if ($_POST['passnuevo']==$_POST['passrepetida']) {
		$nombre = $_POST['usernuevo'];
		$password = $_POST['passnuevo'];
		$passwordencryp= hash_hmac('sha512', $password, 'primeraweb');
		$tipo = $_POST['tipo'];
		$sql = "INSERT INTO usuarios (idusuario,usuario,password,rol) Values ('','$nombre','$passwordencryp','$tipo');";
			if (mysqli_query($conexion,$sql)) {
				echo "Se ha registrado correctamente<br>";
			}else{
				echo " <br> Error: " . $sql . "<br>" . mysqli_error($conexion);
			}
		}else{
			echo "Contraseñas no concuerdan";
		}
	}


	?>
</body>
</html>