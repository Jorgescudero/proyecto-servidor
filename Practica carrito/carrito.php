<!DOCTYPE html>
<html lang="es">
<head>
	<title>Carrito</title>
	<meta charset="utf-8">
	<link href="https://fonts.googleapis.com/css?family=Bangers|Nunito&display=swap" rel="stylesheet"> 
	<style type="text/css">
		html,body{
			background-image: linear-gradient(#e66465, #9198e5);
			width: 100%;
			height: 100%;
			font-family: 'Nunito', sans-serif;
		}
		header{
			font-family: 'Bangers', cursive;
			font-size: 30px;
			margin-left: 25%;
			width: 650px;
			text-align: center;
			background-color: rgba(120, 120, 120, 0.2);
		}
		.carrito{
			display: flex;
			margin-top: 20px;
		}
		.table{
			margin-top: 15px;
			margin-left: 12%;
		}

		.td{
			width: 225px;
			background-color: rgba(120, 120, 120, 0.3);
			border-radius: 15px;
		}

		img{
			margin-top: 10px;
			width: 100px;
			height: 100px;
		}

		.boton1{ 
			width: 150px;
			height: 50px;
			font-size: 17px;
			background-color: rgba(120, 120, 120, 0.3);
			margin-left: 37%;
		}
		.boton{
			width: 150px;
			height: 50px;
			font-size: 17px;
			background-color: rgba(120, 120, 120, 0.3);
		}



		.total{
			background-color: rgba(120, 120, 120, 0.7);
			border-radius: 10px;
			text-align: center;
			font-size: 17px;
			width: 100px;
		}
		.caja{
			margin-left: 50px;
			margin-top: 50px;
		}

		.confirmar{
			background-color: rgba(120, 120, 120, 0.7);
			border-radius: 10px;
			margin-top: 10px;
		}
		
	</style>
</head>
<body>
	<header><h3>Seleccione el articulo que desee comprar</h3></header>
	<div>
		<form action="" method="POST">
			<button type="submit" name="back" class="boton1">Volver al menú</button>
			<button type="submit" name="cerrarsesion" class="boton">Cerrar Sesión</button>
		</form>
	</div>
	<?php
		session_start();
		$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu2'], $_SESSION['pass2'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}
			if (isset($preciototal)==false) {
				$preciototal=0;
			}
			if (isset($_SESSION['total'])==false) {
				$_SESSION['total']=0;
			}
			if (isset($_POST['back'])) {

		header("Location:inicio.php");

	}

	if (isset($_POST['cerrarsesion'])) {

		session_destroy();
			 
		header("Location:index.php");
	}

	?>
	<div class="carrito">
	<form action="control.php" method="POST">
	<table class="table" style="text-align: center;">
		<?php
		$sql= "SELECT * from articulos";
		$resultado= mysqli_query ($conexion, $sql);
		$filas=mysqli_num_rows($resultado);
		$i=0;
		if ($filas>0) {
		while ($registro = mysqli_fetch_row($resultado)) {
			if (isset($_SESSION['cantidad'][$registro[1]])==false) {
				$_SESSION['cantidad'][$registro[1]]=0;
			}
		?>
		<td class="td">
			<img src='<?php  echo "$registro[4]"?>'>
			<p>Nombre: <?php  echo "$registro[1]"?></p>
			<p>Caracteristicas: <?php  echo "$registro[3]"?></p>
			<p>Precio: <?php  echo "$registro[2]"?> €</p>
			<p><button type="submit" name="agregar[]" value=<?php echo "$registro[1]";?>>Agregar</button></p>
		</td>

		<?php
			$i++;
			 if ($i % 3 == 0) {
              echo "</tr><tr>";
            }
			}
		}
		else {
			echo "<tr><td colspan='5'>No tiene ningún articulo introducido</td></tr>";
		}

		?>

	</table>
	</form>
	<div class="caja">
	<table >
		<?php
		if ($_SESSION['total']>0) {
			
		
		?>
		<form action='control.php' method=POST>
    	<table style="margin: auto;background-color: rgba(120, 120, 120, 0.3); border-style: dashed; border-radius: 10px; boder">
    	<tr><th class="total">Producto</th><th class="total">Unidades</th><th class="total">Precio</th><th class="total">Rechazar</th></tr>
    	<?php
    	foreach ($_SESSION['cantidad'] as $key => $value) {
			if ($value>0) {
				$sql1="SELECT precio from articulos where '$key'=articulos.descripcion";
				$resultado1= mysqli_query ($conexion, $sql1);
				$filas1=mysqli_num_rows($resultado1);
				if ($filas1>0) {
				while ($registro1 = mysqli_fetch_row($resultado1)) {
					echo '<tr><td class="total">'. $key .'</td><td class="total">' .$value .'</td><td class="total">'  .($value*$registro1[0]) .'</td><td class="total"><button type="submit" name="quitar[]" value=' .$key .'>Quitar</button></td><tr>';
					$preciototal+=($value*$registro1[0]);
				}
			}
			}
		}
    	echo "<td class='total'>Total: </td><td class='total'>".$preciototal."€</td></tr>";

   	 	echo "</form></table>";

   	 	?>
   	 	<form action="confirmacion.php" method="POST">
  			<button  class='confirmar' type="submit" name="confirmar" value="Confirmar compra">Confirmar compra</button>
  		</form>
  	</div>
	</div>
  <?php

	}

	
	mysqli_close($conexion);
	?>
	
</body>
</html>