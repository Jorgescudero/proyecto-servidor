<?php

session_start();

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<title>Introduccion de articulos <?php echo $_SESSION['rol']; ?></title>
	<link href="https://fonts.googleapis.com/css?family=Bangers|Nunito&display=swap" rel="stylesheet"> 
	<meta charset="utf-8">
	<style type="text/css">
		html,body{
			background-image: linear-gradient(#e66465, #9198e5);
			width: 100%;
			height: 100%;
			font-family: 'Nunito', sans-serif;
		}
		header{
			font-family: 'Bangers', cursive;
			font-size: 30px;
			margin-left: 25%;
			width: 650px;
			text-align: center;
			background-color: rgba(120, 120, 120, 0.2);
		}

		.primero{
			width: 300px;
			margin-left: 37%;
			margin-top: 40px;
			text-align: center;
			background-color: rgba(120, 120, 120, 0.3);
		}

		.boton1{ 
			width: 150px;
			height: 50px;
			font-size: 17px;
			background-color: rgba(120, 120, 120, 0.3);
			margin-left: 37%;
		}

		.boton{
			width: 150px;
			height: 50px;
			font-size: 17px;
			background-color: rgba(120, 120, 120, 0.3);
		}
		.boton2{
			background-color: rgba(120, 120, 120, 0.7);
			font-size: 14px;
		}
	</style>
</head>
<body>
	<header><h3>BIENVENID@ A TECNOMUNDO, Introduzca un articulo si lo desea</h3></header>
	<form  action="" method="POST">
		<button type="submit" name="back" class="boton1">Volver al menu</button>
		<button type="submit" name="cerrar" class="boton">Cerrar sesion</button>
	</form>
	<form action="" method="POST" class="primero">
		<fieldset>
			<legend>Introduce datos</legend>
			<p>Descripcion: <input type="text" name="descripcion" required="required"></p>
			<p>Precio: <input type="number" name="precio" required="required"></p>
			<p>Caracteristicas: <input type="text" name="caracteristicas" required="required"></p>
			<p>Url imagen: <input type="text" name="imagen" required="required"></p>
			<p><input type="submit" name="insertar" value="Insertar" class="boton2"></p>
		</fieldset>
	</form>
	<?php
		if (isset($_POST['insertar'])) {
			if ($_SESSION['rol']=='administrador') {
			$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu1'], $_SESSION['pass1'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}
			$descripcion = $_POST['descripcion'];
			$precio = $_POST['precio'];
			$caracteristicas = $_POST['caracteristicas'];
			$imagen = $_POST['imagen'];
			$sql = "INSERT INTO articulos (id_articulo,descripcion,precio,caracteristicas,imagen) values ('','$descripcion','$precio','$caracteristicas','$imagen');";
			if (mysqli_query($conexion,$sql)) {
				echo "Se ha registrado correctamente<br>";
			}else{
				echo " <br> Error: " . $sql . "<br>" . mysqli_error($conexion);
			}
		}



		if ($_SESSION['rol']=='consultor') {
			$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu2'], $_SESSION['pass2'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}
			$descripcion = $_POST['descripcion'];
			$precio = $_POST['precio'];
			$caracteristicas = $_POST['caracteristicas'];
			$imagen = $_POST['imagen'];
			$sql = "INSERT INTO articulos (id_articulo,descripcion,precio,caracteristicas,imagen) values ('','$descripcion','$precio','$caracteristicas','$imagen');";
			if (mysqli_query($conexion,$sql)) {
				echo "Se ha registrado correctamente<br>";
			}else{
				echo " <br> Error: " . $sql . "<br>" . mysqli_error($conexion);
			}
		}
		}

		if (isset($_POST['back'])) {
			header("Location:inicio.php");		
		}

		if (isset($_POST['cerrar'])) {
			header("Location:index.php");
			session_destroy();
		}


		



	?>
</body>
</html>