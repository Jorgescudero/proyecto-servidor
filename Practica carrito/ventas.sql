-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-01-2020 a las 13:24:18
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ventas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos`
--

CREATE TABLE `articulos` (
  `id_articulo` int(11) NOT NULL,
  `descripcion` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `precio` int(10) NOT NULL,
  `caracteristicas` varchar(150) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `imagen` varchar(150) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `articulos`
--

INSERT INTO `articulos` (`id_articulo`, `descripcion`, `precio`, `caracteristicas`, `imagen`) VALUES
(1, 'Alfombrilla', 20, '20cmx20cm, deslizante', 'alfombrilla.jpg'),
(2, 'Iphone', 1100, '128 GB de memoria, resolucion 4K', 'movil.jpg'),
(3, 'PS4', 300, '1 T de memoria', 'ps4.jpg'),
(4, 'Raton', 50, 'buena movilidad,buen deslizamiento', 'raton.jpg'),
(5, 'Televisor', 700, '4K, curva, sonido stereo', 'televisor.jpg'),
(6, 'USB', 10, '1 T de memoria', 'usb.jpg'),
(7, 'alfombrilla-multi', 30, 'gama de colores excelente,buen deslizamine.', 'Alfombrilla2.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `idCliente` int(11) NOT NULL,
  `idProducto` int(11) NOT NULL,
  `Fecha` datetime NOT NULL,
  `Cantidad` int(11) NOT NULL,
  `Precio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `compras`
--

INSERT INTO `compras` (`idCliente`, `idProducto`, `Fecha`, `Cantidad`, `Precio`) VALUES
(11, 1, '2020-01-23 10:22:28', 1, 20),
(11, 1, '2020-01-24 09:28:30', 2, 20),
(11, 1, '2020-01-24 09:29:08', 2, 20),
(11, 2, '2020-01-23 10:22:28', 1, 1100),
(11, 2, '2020-01-24 09:25:21', 2, 1100),
(11, 2, '2020-01-24 09:25:24', 2, 1100),
(11, 2, '2020-01-24 12:21:46', 1, 1100),
(11, 2, '2020-01-24 12:21:48', 1, 1100),
(11, 3, '2020-01-09 00:00:00', 1, 20),
(11, 3, '2020-01-24 12:21:46', 1, 300),
(11, 3, '2020-01-24 12:21:48', 1, 300),
(11, 5, '2020-01-23 12:18:40', 1, 700),
(11, 5, '2020-01-27 10:55:05', 1, 700),
(12, 1, '2020-01-24 08:52:26', 1, 20),
(12, 1, '2020-01-24 08:54:13', 1, 20),
(12, 2, '2020-01-24 08:43:00', 1, 1100),
(12, 3, '2020-01-24 08:52:26', 1, 300),
(12, 3, '2020-01-24 08:54:13', 1, 300),
(12, 3, '2020-01-24 09:29:21', 2, 300),
(12, 3, '2020-01-24 09:29:54', 2, 300),
(12, 5, '2020-01-24 08:52:26', 1, 700),
(12, 5, '2020-01-24 08:54:13', 1, 700),
(13, 4, '2020-01-24 13:22:25', 1, 50),
(13, 4, '2020-01-24 13:22:33', 1, 50),
(13, 4, '2020-01-24 13:22:48', 3, 50),
(13, 4, '2020-01-24 13:31:40', 3, 50),
(13, 5, '2020-01-24 13:22:49', 1, 700),
(13, 5, '2020-01-24 13:31:40', 1, 700);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idusuario` int(11) NOT NULL,
  `usuario` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `rol` varchar(15) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idusuario`, `usuario`, `password`, `rol`) VALUES
(1, 'pepe', 'pepe', 'consultor'),
(2, 'juan', 'juan', 'consultor'),
(3, 'ana', 'ana', 'administrador'),
(4, 'maria', 'maria', 'administrador'),
(10, 'jorge', '820f92fbdc6504dbb22d1426acba0e77d163c4175422b8a65999855c5762ef1ed115e5dd1f3a274a7acc5520c14de1d6259f55584d90f4bc10aafae444ee5e0a', 'administrador'),
(11, 'hector', 'fcc8553416b0deb598f336fcaa798d7724ddcd35556ee8a999018ae036710a60ed887c46d0c7b02f804f9819ea636174b503d71174ded7c30f790db1dddcff3d', 'consultor'),
(12, 'david', '577bf673e344431013dc7ef184119770790681442cf6677eb88a591ac3aa1e639e287505871f2e97705124da4ffbc6331a0412e6bd0ce04ca3967b31fdc4f5ea', 'consultor'),
(13, 'oscar', '8f2f84ac9f096d063c9a9f4c7b65ddc7fefb87776ebe5c1edd2963ec07146f35558dc7bb15e52efb0e1da94b9af2166934eb55975313f7a36b2ce4cb2bbb7322', 'consultor');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD PRIMARY KEY (`id_articulo`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`idCliente`,`idProducto`,`Fecha`),
  ADD KEY `idProducto` (`idProducto`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idusuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articulos`
--
ALTER TABLE `articulos`
  MODIFY `id_articulo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `compras`
--
ALTER TABLE `compras`
  ADD CONSTRAINT `compras_ibfk_1` FOREIGN KEY (`idCliente`) REFERENCES `usuarios` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `compras_ibfk_2` FOREIGN KEY (`idProducto`) REFERENCES `articulos` (`id_articulo`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
