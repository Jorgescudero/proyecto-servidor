<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<title>Listado de articulos <?php echo $_SESSION['rol']; ?></title>
	<meta charset="utf-8">
	<link href="https://fonts.googleapis.com/css?family=Bangers|Nunito&display=swap" rel="stylesheet"> 
	<style type="text/css">
		html,body{
			background-image: linear-gradient(#e66465, #9198e5);
			width: 100%;
			height: 100%;
			font-family: 'Nunito', sans-serif;
		}
		header{
			font-family: 'Bangers', cursive;
			font-size: 30px;
			margin-left: 25%;
			width: 650px;
			text-align: center;
			background-color: rgba(120, 120, 120, 0.2);
		}
		.boton1{ 
			width: 150px;
			height: 50px;
			font-size: 17px;
			background-color: rgba(120, 120, 120, 0.3);
			margin-left: 37%;
		}
		.boton{
			width: 150px;
			height: 50px;
			font-size: 17px;
			background-color: rgba(120, 120, 120, 0.3);
		}

		table{
			width: 600px;
			margin-left: 27%;
			margin-top: 30px;
			background-color: rgba(120, 120, 120, 0.5);
		}
	</style>
</head>
<body>
	<header><h3>Articulos dentro del catalogo de TecnoMundo</h3></header>

	<?php

	if ($_SESSION['rol']=='administrador') {

	?>

	<div>
		<form action="" method="POST">
			<button type="submit" name="back" class="boton1">Volver al menú</button>
			<button type="submit" name="cerrarsesion" class="boton">Cerrar Sesión</button>
		</form>
	</div>
	<?php

		$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu1'], $_SESSION['pass1'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

	?>

	<table border="1" style="text-align: center;">
		<tr>
			<th>ID</th>
			<th>Descripcion</th>
			<th>Precio</th>
			<th>Caracteristicas</th>
		</tr>

		<?php

		$sql= "SELECT * from articulos";
		$resultado= mysqli_query ($conexion, $sql);
		$filas=mysqli_num_rows($resultado);
		if ($filas>0) {
		while ($registro = mysqli_fetch_row($resultado)) {

		?>
	
		<tr>
			<td><?php echo $registro[0]; ?></td>
			<td><?php echo $registro[1]; ?></td>
			<td><?php echo $registro[2]; ?></td>
			<td><?php echo $registro[3]; ?></td>
		</tr>

		<?php

			}
		}
		else {
			echo "<tr><td colspan='5'>No tiene ningún articulo introducido</td></tr>";
		}}

		?>

		<?php

	if ($_SESSION['rol']=='consultor') {

	?>

	<div>
		<form action="" method="POST">
			<button type="submit" name="back">Volver al menú</button>
			<button type="submit" name="cerrarsesion">Cerrar Sesión</button>
		</form>
	</div>
	<?php

		$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu2'], $_SESSION['pass2'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

	?>

	<table border="1" style="text-align: center;">
		<tr>
			<th>ID</th>
			<th>Descripcion</th>
			<th>Precio</th>
			<th>Caracteristicas</th>
		</tr>

		<?php

		$sql= "SELECT * from articulos";
		$resultado= mysqli_query ($conexion, $sql);
		$filas=mysqli_num_rows($resultado);
		if ($filas>0) {
		while ($registro = mysqli_fetch_row($result)) {

		?>
	
		<tr>
			<td><?php echo $registro[0]; ?></td>
			<td><?php echo $registro[1]; ?></td>
			<td><?php echo $registro[2]; ?></td>
			<td><?php echo $registro[3]; ?></td>
		</tr>

		<?php

			}
		}
		else {
			echo "<tr><td colspan='5'>No tiene ningún articulo introducido</td></tr>";
		}}

		?>

	</table>

<?php	

	if (isset($_POST['back'])) {

		header("Location:inicio.php");

	}

	if (isset($_POST['cerrarsesion'])) {

		session_destroy();
			 
		header("Location:index.php");
	}
	mysqli_close($conexion);
	?>
</body>
</html>