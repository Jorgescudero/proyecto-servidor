<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Concesionario</title>
  <style>
    body {
      font-family: Arial, Helvetica, sans-serif;
      width: 600px;
      margin: auto;
      margin-top: 200px;
      text-align: center;

    }

    form,
    input {
      font-size: 30px;
    }

    input {
      text-align: center;
      font-size: 20px;
    }

    #error {
      color: red;
    }
  </style>
</head>

<body>
  <?php
  session_start();
  if (isset($_POST["acceder"])) {
    $dni = $_POST["dni"];
    $conexion = mysqli_connect('localhost', 'cliente', 'cliente', 'concesionario1');
    if (mysqli_connect_errno()) {
      printf("Conexión fallida: %s\n", mysqli_connect_error());
      exit();
    } else {

      $sql = 'SELECT NOMBRECLI, ROL from clientes where DNICLI="' . $dni . '"';

      $resultado = mysqli_query($conexion, $sql);

      $row = mysqli_fetch_array($resultado, MYSQLI_ASSOC);
      
      if ($row == null) {
        $error = "El DNI introducido no está registrado";
      } else {
        $_SESSION["datos"] = $row;
        header('Location: ingreso.php');
      }
      mysqli_close($conexion);
    }
  } else  {
    $error = "" ;
  }
  ?>

  <h1>ACCESO</h1>

  <form action="" method="POST">
    <label for="dni">INGRESE SU DNI</label>
    <br>
    <input type="text" name="dni" value="<?php if (isset($_POST["acceder"])) echo $_POST["dni"] ?>">
    <br><br>
    <input type="submit" name="acceder" value="ACCEDER">
  </form>
  <br><br>
  <span name="error" id="error"><?php echo $error ?></span>

</body>

</html>