<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Concesionario</title>
  <style>
    body {
      font-family: Arial, Helvetica, sans-serif;
      width: 600px;
      margin: auto;
      margin-top: 200px;
      text-align: center;
    }

    h1 {
      font-size: 35px;
    }

    h3 {
      font-size: 30px;
    }


    form,
    input,
    select {
      font-size: 20px;
    }

    #aviso1 {
      color: red;
    }

    #aviso2 {
      color: green;
    }
  </style>
</head>

<body>
  <?php
  session_start();
  $aviso1 = "";
  $aviso2 = "";
  if ($_SESSION["datos"]["ROL"] == "trabajador") {
    $conexion = mysqli_connect('localhost', 'trabajador', 'trabajador', 'concesionario1');
  } else {
    $conexion = mysqli_connect('localhost', 'cliente', 'cliente', 'concesionario1');
  }

  if (isset($_POST["alta"])) {
    $codigo = $_POST["codigo"];
    $marca = $_POST["marca"];
    $modelo = $_POST["modelo"];
    $nombre = $_POST["nombre"];

    $sql = "INSERT INTO coches VALUES ($codigo, $marca , '$modelo' , '$nombre' )";

    if (mysqli_multi_query($conexion, $sql)) {
      $aviso2 = "El alta del vehículo se ha registrado correctamente";
    } else {
      $aviso1 = "Error: no tiene permisos para ejecutar un alta";
    }
  }

  ?>
  <h1>Bienvenido <?php echo $_SESSION["datos"]["NOMBRECLI"] ?> <br>Has ingresado como <?php echo $_SESSION["datos"]["ROL"] ?></h1>
  <br><br>

  <h3>ALTA DE VEHÍCULO</h3>
  <form action="" method="POST">
    <label for="codigo">CÓDIGO</label>
    <input type="text" name="codigo" value="<?php if (isset($_POST["alta"])) echo $_POST["codigo"] ?>">
    <br><br>
    <label for="marca">MARCA</label>
    <select name="marca">
      <option value="1111" <?php if (isset($_POST["acceder"]) && $_POST["marca"] == 1111) echo 'selected="selected" '; ?>>SEAT</option>
      <option value="2222" <?php if (isset($_POST["acceder"]) && $_POST["marca"] == 2222) echo 'selected="selected" '; ?>>RENAULT</option>
      <option value="3333" <?php if (isset($_POST["acceder"]) && $_POST["marca"] == 3333) echo 'selected="selected" '; ?>>NISSAN</option>
      <option value="4444" <?php if (isset($_POST["acceder"]) && $_POST["marca"] == 4444) echo 'selected="selected" '; ?>>FORD</option>
      <option value="5555" <?php if (isset($_POST["acceder"]) && $_POST["marca"] == 5555) echo 'selected="selected" '; ?>>OPEL</option>
    </select>
    <br><br>
    <label for="modelo">MODELO</label>
    <input type="text" name="modelo" value="<?php if (isset($_POST["alta"])) echo $_POST["modelo"] ?>">
    <br><br>
    <label for="nombre">NOMBRE</label>
    <input type="text" name="nombre" value="<?php if (isset($_POST["alta"])) echo $_POST["nombre"] ?>">
    <br><br>
    <input type="submit" name="alta" value="DAR DE ALTA">
  </form>
  <span name="aviso" id="aviso1"><?php echo $aviso1 ?></span>
  <span name="aviso" id="aviso2"><?php echo $aviso2 ?></span>

  <?php
  mysqli_close($conexion);
  ?>
</body>

</html