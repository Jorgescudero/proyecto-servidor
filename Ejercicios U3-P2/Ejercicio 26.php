<!DOCTYPE html>
<html>
<head>
	<title>Ejercicio 25</title>
</head>
<body>
	<?php
	$simpsons= array("Padre"=>"Homer","Madre"=>"Marge","Hijos"=>array('Hijo' => "Bart", 'Hija' => "Lisa",'Bebe' => "Maggie"));
	$griffin= array("Padre"=>"Peter","Madre"=>"Lois","Hijos"=>array('Hijo' => "Chris", 'Hija' => "Meg",'Bebe' => "Stewie"));
	
	function mostrarfamilia($familia){
		echo "<ul>";
		foreach ($familia as $i=> $persona) {
			if (is_array($persona)) {
				foreach ($persona as $j => $hijos) {
					echo "<li>".$j." : " .$hijos ."</li>";
				}
			}else{
				echo "<li>".$i.": " .$persona ."</li>";
			}
		}
		echo "</ul>";
	}

	?>

	<h3>Familia Simpsons</h3>
	<?php mostrarfamilia($simpsons);?>
	<br><br>

	<h3>Familia Simpsons</h3>
	<?php mostrarfamilia($griffin);?>



</body>
</html>