<!DOCTYPE html>
<html>
<head>
	<title>Ejercicio 18</title>
</head>
<body>
	<?php
	$cliente = Array("Cliente1"=>"HTML","Cliente2"=>"JavaScript","Cliente3"=>"Flash");

	$servidor = Array("Servidor1"=>"CGI","Servidor2"=>"ASP","Servidor3"=>"PHP");

	$lenguajes[]=$cliente;
	$lenguajes[]=$servidor;
	?>

	<table border="1" bordercolor="grey" cellpadding="2" cellspacing="2">
		<th colspan="2">Lenguajes</th>
		<?php
		foreach ($lenguajes as $dato) {
			foreach ($dato as $i => $len) {
				echo "<tr><td>".$i."</td><td>".$len."</td></tr>";
			}
		}

		?>
	</table>

</body>
</html>