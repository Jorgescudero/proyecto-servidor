<!DOCTYPE html>
<html>
<head>
	<title>Ejercicio 25</title>
</head>
<body>
	<?php
	$Madrid = array("Nombre" => "Pedro" , "Edad" => 32 , "Telefono" => "91-999.99.99");
	$Barcelona = array("Nombre" => "Susana" , "Edad" => 34 , "Telefono" => "93-000.00.00");
	$Toledo = array("Nombre" => "Sonia" , "Edad" => 42 , "Telefono" => "925-09.09.09");
	$amigos= array ("Madrid" => $Madrid,"Barcelona" => $Barcelona,"Toledo" => $Toledo);
	?>

	<ul>
		<?php
		foreach ($amigos as $i => $ciudad){
			echo $i."<ul>";
			foreach ($ciudad as $j => $amigo) {
				echo "<li>" .$j."--".$amigo."</li>";
			}
			echo "</ul>";
			}?>
	</ul>


</body>
</html>