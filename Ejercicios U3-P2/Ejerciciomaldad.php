<!DOCTYPE html>
<html>
<head>
	<title>Ejercicio 25</title>
</head>
<body>
	<?php
	$v1= array('v10' => 'A','v11' => 'B','v12' => array('v121' => 'AC','v122' => 'AC2'),'v13' => array('v131' => array('v1311' => 'AAD' ,'v1312' => 'AAD2'), 'v132' => array('v1321' => 'AAE','v1322' => 'AAE2')) );
	$v2= array('v20' => array('v201' => 'AF', 'v202' => 'AF2'),'v21'=>'G','v22' => array('v221' => 'AH' ,'v222' => 'AH2'),'v23'=>array('v231' => array('2311' => 'AAI' ,'2311' => 'AAI2'), 'v232' => array('v2321' => 'AAJ','v2322' => 'AAJ2' )) );
	$v3= array('v30' => 'K','v31' => 'L','v32' => array('v321' => 'AM','v322' => 'AM2' ),'v33'=>array('v331' => array('v3311' => 'AAN','v3312' => 'AAN2' ), 'v332' => array('v3321' => 'AAÑ','v3322' => 'AAÑ2' )) );
	$v=array('v1' => $v1,'v2' => $v2,'v3' => $v3 );
	
	function mostrarfamilia($familia){
		echo "<ul>";
		foreach ($familia as $i=> $persona) {
			if (is_array($persona)) {
				foreach ($persona as $j => $hijos) {
					if(is_array($hijos)){
						foreach ($hijos as $k => $nietos) {
							if (is_array($nietos)) {
								foreach ($nietos as $a => $bisnietos) {
									echo "<li>".$a." : " .$bisnietos ."</li>";
								}
							}else{
							echo "<li>".$k." : " .$nietos ."</li>";
						}
					}}else{
					echo "<li>".$j." : " .$hijos ."</li>";
				}}
			}else{
				echo "<li>".$i.": " .$persona ."</li>";
			}
		}
		echo "</ul>";
	}

	?>

	<h3>Familia V</h3>
	<?php mostrarfamilia($v);?>
	<br><br>



</body>
</html>