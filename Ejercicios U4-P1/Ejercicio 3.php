<?php
	
	class empleado{
		public $nombre;
		public $sueldo;

		public function __construct(){
			echo 'The class "', __CLASS__, '" was initiated!<br />';
		}

		public function __destruct(){
			echo 'The class "', __CLASS__, '" was destroyed.<br />';
		}

		function get_nombre(){
			return $this->nombre;
		}

		function get_sueldo(){
			return $this->sueldo;
		}

		public function sueldo($persona){
			if($persona->get_sueldo()<3000){
				echo $persona->get_nombre() .":No paga impuestos <br>";
			}else{
				echo $persona->get_nombre() .":Debe pagar impuestos <br>";
			}
		}
	}

	$persona1 = new empleado;
	$persona1->nombre = 'Luis';
	$persona1->sueldo = 2500;
	$persona1->sueldo($persona1);
	var_dump($persona1);
	echo("<br>");

	$persona2 = new empleado;
	$persona2->nombre = 'Carla';
	$persona2->sueldo = 5000;
	$persona2->sueldo($persona2);
	var_dump($persona2);
	echo("<br>");
	?>