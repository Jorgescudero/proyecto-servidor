<?php
	
	class Libro{
		public $titulo;
		public $autor;
		public $añodepublicacion;
		public $hojas;
		public $editorial;

		public function __construct(){
			echo 'The class "', __CLASS__, '" was initiated!<br />';
		}

		public function __destruct(){
			echo 'The class "', __CLASS__, '" was destroyed.<br />';
		}

		function get_titulo(){
			return $this->titulo;
		}

		function get_autor(){
			return $this->autor;
		}

		function get_añodepublicacion(){
			return $this->añodepublicacion;
		}

		function get_hojas(){
			return $this->hojas;
		}

		public function datos($libro){
			foreach ($libro as $key => $value) {
				echo $key ." : " .$value ."<br>";
			}
		}
	}

	$libro = new Libro;
	$libro->titulo='Los pilares de la tierra';
	$libro->autor='Ken Follet';
	$libro->añodepublicacion = 2001;
	$libro->hojas = 1040;
	$libro->editorial = 'Plaza & Janes Editores';
	$libro->datos($libro);
	var_dump($libro);


	?>