<?php
	
	class persona{
		public $nombre;
		public $edad;

		public function __construct(){
			echo 'The class "', __CLASS__, '" was initiated!<br />';
		}

		public function __destruct(){
			echo 'The class "', __CLASS__, '" was destroyed.<br />';
		}

		function get_nombre(){
			return $this->nombre;
		}

		function get_edad(){
			return $this->edad;
		}

		public function datos($persona){
			foreach ($persona as $key => $value) {
				echo $key ." : " .$value ."<br>";
			}
		}
	}

	class empleado extends persona{
		public $sueldo;

		function get_sueldo(){
			return $this->sueldo;
		}
	}

	$persona1 = new persona;
	$persona1->nombre = 'Rodriguez Pablo';
	$persona1->edad = 24;


	$empleado1 = new empleado;
	$empleado1->nombre = 'Gonzalez Ana';
	$empleado1->edad = 32;
	$empleado1->sueldo = 2400;

	
	echo "Datos personales de la persona:<br>";
	$persona1->datos($persona1);
	echo "Datos personales y sueldo del empleado: <br>";
	$empleado1->datos($empleado1);
	?>