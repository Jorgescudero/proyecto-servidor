<?php
	session_name("Ejercicio-1");
	session_start();

	if (!isset($_SESSION["numero"])) {
		$_SESSION["numero"]=0;
	}
	?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	
	<form action="controlEjercicio1.php" method="POST">
		<p>
			<button type="submit" name="accion" value="bajar">-</button>
			<?php echo "<span>" .$_SESSION['numero'] ."</span>"; ?>
			<button type="submit" name="accion" value="subir">+</button>
		</p>
		<button type="submit" name="accion" value="cero">Poner a cero</button>
		
	</form>

</body>
</html>