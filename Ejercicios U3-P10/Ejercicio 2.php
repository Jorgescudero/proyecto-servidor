<?php
	session_name("Ejercicio-2");
	session_start();

	if (!isset($_SESSION["posicion"])) {
		$_SESSION["posicion"]=150;
	}
	?>
<!DOCTYPE html>
<html>
<head>
	<title>Ejercicio 2</title>
	<style type="text/css">
		form{
			margin-left: 300px;
		}
	</style>
</head>
<body>
	
	<form action="controlEjercicio2.php" method="POST">
		<p>
			<button type="submit" name="accion" value="izquierda"><-</button>
			<button type="submit" name="accion" value="derecha">-></button>
		</p>
		<svg>
		<line x1= "-300" y1="10" x2="300" y2="10" stroke="black" stroke-width="5"/>
		<?php echo "<circle cx=" .$_SESSION['posicion'] ." cy=\"10\" r=\"8\" fill=\"red\"/>"; ?>
		</svg>
		<button type="submit" name="accion" value="centro">Volver al centro</button>
		
	</form>
</body>
</html>