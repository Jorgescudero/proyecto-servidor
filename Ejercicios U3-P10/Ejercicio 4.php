<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Ejercicio 4</title>

</head>

<body>
  <?php
  session_name("ejercicio-4");
  session_start();

  if (!isset($_SESSION["a"]) || !isset($_SESSION["b"])) {
    $_SESSION["a"] = $_SESSION["b"] = 0;
  }
  ?>
    <form action="controlEjercicio4.php" method="POST">
      
      <button type="submit" name="accion" value="a" style="font-size: 60px; line-height: 50px; color: #33f3ff;">✔</button>
            <?php
            echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" \n";
            echo "width=\"$_SESSION[a]\" height=\"50\">\n";
            echo "<line x1=\"0\" y1=\"25\" x2=\"$_SESSION[a]\" y2=\"25\" stroke=\"#33f3ff\" stroke-width=\"50\" />\n";
            echo "</svg>\n";
            ?><br>
        <button type="submit" name="accion" value="b" style="font-size: 60px; line-height: 50px; color: #ff5733">✔</button>
            <?php
            echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" \n";
            echo "width=\"$_SESSION[b]\" height=\"50\">\n";
            echo "<line x1=\"0\" y1=\"25\" x2=\"$_SESSION[b]\" y2=\"25\" stroke=\"#ff5733\" stroke-width=\"50\" />\n";
            echo "</svg>\n";
            ?><br>
            <button style="font-size:50px; height:75px" type="submit" name="accion" value="cero">&#9194</button>
            <br>
    </form>
  </div>
</body>

</html>