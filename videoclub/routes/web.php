<?php

use Illuminate\Support\Facades\Route;

//Rutas de login y registro
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

//Rutas del proyecto
Route::get('/', 'HomeController@getHome');

Route::group(['middleware' => 'auth'], function() {
	Route::get('catalog', 'CatalogController@getIndex');
	Route::get('catalog/show/{id}', 'CatalogController@getShow');
	Route::put('catalog/show/{id}', 'CatalogController@putAlquilaDevuelve');
	Route::get('catalog/create', 'CatalogController@getCreate');
	Route::post('catalog/create', 'CatalogController@postCreate');
	Route::get('catalog/edit/{id}', 'CatalogController@getEdit');
	Route::put('catalog/edit/{id?}', 'CatalogController@putEdit');
	Route::get('catalog/delete/{id}', 'CatalogController@getDelete');
	Route::delete('catalog/delete/{id}', 'CatalogController@deleteMovie');
	Route::get('catalog/byyear', 'CatalogController@movieYears');
});