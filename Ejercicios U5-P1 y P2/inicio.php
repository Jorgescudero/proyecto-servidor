<?php

session_start();
$_SESSION['servidor']='localhost';
$_SESSION['basedatos']='ventas';
$_SESSION['usu1']='Administrador';
$_SESSION['pass1']='Administrador';
$_SESSION['usu2']='Consultor';
$_SESSION['pass2']='Consultor';

?>

<!DOCTYPE html>
<html>
<head>
	
</head>
<body>
	<?php

	if ($_SESSION['rol']=='consultor') {
	  	header("Location:carrito.php");
	  }  

	?>
	<title>Inicio <?php echo $_SESSION['rol']; ?></title>
	<h1>Bienvenido/a <?php echo $_SESSION['user']; ?>, se ha identificado como <?php echo $_SESSION['rol'] ?></h1>
	<form action="" method="POST">
		<fieldset>
			<legend>Menú de opciones</legend>
			<p><button type="submit" name="introducir">Introducir articulos</button></p>
			<p><button type="submit" name="visualizar">Visualizar Articulos</button></p>
		</fieldset>
	</form>

	<?php

			if (isset($_POST['introducir'])) {

				header("Location:introducirarticulos.php");

			}

			if (isset($_POST['visualizar'])) {

				header("Location:visualizararticulos.php");

			}
	?>
</body>
</html>