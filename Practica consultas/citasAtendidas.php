<?php

session_start();

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Citas atendidas <?php echo $_SESSION['rol']; ?></title>
	<meta charset="utf-8">
	<style type="text/css">
		body{
			background-color: lightgrey;
			text-align: center;
		}
		form{
			background-color: white;
			text-align: center;
			width: 400px;
			margin-left: 32%;
		}
		input{
			border-color: darkgrey;
		}
		button{
			width: 250px;
			background-color: darkgrey;
			color: white;
			border-color: black;
		}
		table{
			background-color: white;
			margin-left: 27%;
			width: 500px;
		}
	</style>
</head>
<body>
	
	<?php

	if ($_SESSION['rol']=='Medico') {

	?>
	
	<h1>Bienvenido/a, se ha identificado como <?php echo $_SESSION['rol'] ?></h1>
	<div>
		<form action="" method="POST">
			<button type="submit" name="back">Volver al menú</button>
			<button type="submit" name="cerrarsesion">Cerrar Sesión</button>
		</form>
	</div>

	<?php	
		$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu2'], $_SESSION['pass2'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

	?>
	
	<table border="1" style="text-align: center;">
		<tr>
			<th>Fecha</th>
			<th>Hora</th>
			<th>Paciente</th>
			<th>Consultorio</th>
			<th>Observaciones</th>
		</tr>

		<?php

		$nif=$_SESSION['nif'];

		$sql="SELECT citas.citFecha, citas.citHora, pacientes.pacNombres, pacientes.pacApellidos, consultorios.conNombre, citas.citObservaciones FROM citas, pacientes, consultorios WHERE citas.citMedico='$nif' AND citas.citEstado='Atendido' AND citas.citPaciente=pacientes.dniPac AND citas.citConsultorio=consultorios.idConsultorio;";
		$result = mysqli_query ($conexion, $sql);
		$filas=mysqli_num_rows($result);
		if ($filas>0) {
			while ($registro = mysqli_fetch_row($result)) {
				
		?>

		<tr>
			<td><?php echo $registro[0]; ?></td>
			<td><?php echo $registro[1]; ?></td>
			<td><?php echo $registro[2]." ".$registro[3]; ?></td>
			<td><?php echo $registro[4]; ?></td>
			<td><?php echo $registro[5]; ?></td>
		</tr>

		<?php

			}
		}
		else {
			echo "<tr><td colspan='5'>No hay ninguna cita atendida</td></tr>";
		}

		?>

	</table>

	<?php		

	}

	if ($_SESSION['rol']=='Asistente') {

	?>
	
	<h1>Bienvenido/a <?php echo $_SESSION['user']; ?>, se ha identificado como <?php echo $_SESSION['rol'] ?></h1>
	<div>
		<form action="" method="POST">
			<button type="submit" name="back">Volver al menú</button>
			<button type="submit" name="cerrarsesion">Cerrar Sesión</button>
		</form>
	</div>

	<?php	
		$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu3'], $_SESSION['pass3'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}
	?>
	
	<table border="1" style="text-align: center;">
		<tr>
			<th>Fecha</th>
			<th>Hora</th>
			<th>Paciente</th>
			<th>Médico</th>
			<th>Consultorio</th>
			<th>Observaciones</th>
		</tr>

		<?php

		$sql="SELECT citas.citFecha,citas.citHora,pacientes.pacNombres,pacientes.pacApellidos,medicos.medNombres,medicos.medApellidos,consultorios.conNombre,citas.citObservaciones FROM citas,pacientes,medicos,consultorios WHERE citas.citEstado='Atendido' AND citas.citPaciente=pacientes.dniPac AND citas.citMedico=medicos.dniMed AND citas.citConsultorio=consultorios.idConsultorio;";
		$result = mysqli_query ($conexion, $sql);
		$filas=mysqli_num_rows($result);
		if ($filas>0) {
			while ($registro = mysqli_fetch_row($result)) {

		?>

		<tr>
			<td><?php echo $registro[0]; ?></td>
			<td><?php echo $registro[1]; ?></td>
			<td><?php echo $registro[2]." ".$registro[3]; ?></td>
			<td><?php echo $registro[4]." ".$registro[5]; ?></td>
			<td><?php echo $registro[6]; ?></td>
			<td><?php echo $registro[7]; ?></td>
		</tr>	

		<?php

			}
		}
		else {
			echo "<tr><td colspan='6'>No hay ninguna cita atendida</td></tr>";
		}

		?>

	</table>	

	<?php	

	}

	if (isset($_POST['back'])) {

		header("Location:inicio.php");

	}

	if (isset($_POST['cerrarsesion'])) {

		session_destroy();
			 
		header("Location:Proyectofinal1EVA.php");
	}
	mysqli_close($conexion);
	?>
</body>
</html>