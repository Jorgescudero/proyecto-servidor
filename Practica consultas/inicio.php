<?php

session_start();
$_SESSION['servidor']='localhost';
$_SESSION['basedatos']='consultas';
$_SESSION['usu1']='administrador';
$_SESSION['pass1']='administrador';
$_SESSION['usu2']='medico';
$_SESSION['pass2']='medico';
$_SESSION['usu3']='asistente';
$_SESSION['pass3']='asistente';
$_SESSION['usu4']='paciente';
$_SESSION['pass4']='paciente';

?>

<!DOCTYPE html>
<html>
<head>
	<title>Inicio <?php echo $_SESSION['rol']; ?></title>
	<meta charset="utf-8">
	<style type="text/css">
		<style type="text/css">
		h1{
			text-align: center;
		}
		body{
			background-color: lightgrey;
			text-align: center;
		}
		form{
			background-color: white;
			text-align: center;
			width: 400px;
			margin-left: 32%;
		}
		input{
			border-color: darkgrey;
		}
		button{
			width: 250px;
			background-color: darkgrey;
			color: white;
			border-color: black;
		}
	</style>
</head>
<body class="body">

	<?php

	if (isset($_SESSION['nif']) && isset($_SESSION['rol'])) {

		if ($_SESSION['rol']=="Administrador") {
			$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu1'], $_SESSION['pass1'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

	?>
	
	<h1>Bienvenido/a <?php echo $_SESSION['user']; ?>, se ha identificado como <?php echo $_SESSION['rol'] ?></h1>
	<form action="" method="POST">
		<fieldset>
			<legend>Menú de opciones</legend>
			<p><button type="submit" name="altapaciente">Alta Paciente</button></p>
			<p><button type="submit" name="altamedico">Alta Médico</button></p>
			<p><button type="submit" name="cerrarsesion">Cerrar Sesión</button></p>
		</fieldset>
	</form>

	<?php

			if (isset($_POST['altapaciente'])) {

				header("Location:altapaciente.php");

			}

			if (isset($_POST['altamedico'])) {

				header("Location:altamedico.php");

			}

			if (isset($_POST['cerrarsesion'])) {

				session_destroy();
		 
				header("Location:index.php");
			}		

		}

		if ($_SESSION['rol']=='Medico') {
			$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu2'], $_SESSION['pass2'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

			$sql="SELECT medNombres,medApellidos FROM medicos WHERE dniMed='".$_SESSION['nif']."';";
			$result = mysqli_query ($conexion, $sql);

	?>
	
	<h1>Bienvenido/a <?php echo $_SESSION['user']?>, se ha identificado como <?php echo $_SESSION['rol'] ?></h1>
	<form action="" method="POST">
		<fieldset>
			<legend>Menú de opciones</legend>
			<p><button type="submit" name="citasa">Ver Citas Atendidas</button></p>
			<p><button type="submit" name="citasp">Ver Citas Pendientes</button></p>
			<p><button type="submit" name="vpac">Ver Pacientes</button></p>
			<p><button type="submit" name="cerrarsesion">Cerrar Sesión</button></p>
		</fieldset>
	</form>	

	<?php

			if (isset($_POST['citasa'])) {

				header("Location:citasAtendidas.php");

			}

			if (isset($_POST['citasp'])) {

				header("Location:citasPendientes.php");

			}

			if (isset($_POST['vpac'])) {

				header("Location:listadoPacientes.php");

			}

			if (isset($_POST['cerrarsesion'])) {

					session_destroy();
			 
					header("Location:index.php");
			}	

		}

		if ($_SESSION['rol']=='Asistente') {
			$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu3'], $_SESSION['pass3'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

	?>
	
	<h1>Bienvenido/a <?php echo $_SESSION['user']; ?>, se ha identificado como <?php echo $_SESSION['rol'] ?></h1>
	<form action="" method="POST">
		<fieldset>
			<legend>Menú de opciones</legend>
			<p><button type="submit" name="citasa">Ver Citas Atendidas</button></p>
			<p><button type="submit" name="nuevacita">Nueva Cita</button></p>
			<p><button type="submit" name="altapaciente">Alta Paciente</button></p>
			<p><button type="submit" name="vpac">Ver Pacientes</button></p>
			<p><button type="submit" name="cerrarsesion">Cerrar Sesión</button></p>
		</fieldset>
	</form>	

	<?php

			if (isset($_POST['citasa'])) {

				header("Location:citasAtendidas.php");

			}

			if (isset($_POST['nuevacita'])) {

				header("Location:nuevaCita.php");

			}

			if (isset($_POST['altapaciente'])) {

				header("Location:altapaciente.php");

			}

			if (isset($_POST['vpac'])) {

				header("Location:listadoPacientes.php");

			}

			if (isset($_POST['cerrarsesion'])) {

					session_destroy();
			 
					header("Location:index.php");
			}

		}

		if ($_SESSION['rol']=='Paciente') {
			$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu4'], $_SESSION['pass4'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

			$sql="SELECT pacNombres,pacApellidos FROM pacientes WHERE dniPac='".$_SESSION['nif']."';";
			$result = mysqli_query ($conexion, $sql);

	?>

	<h1>Bienvenido/a <?php echo $_SESSION['user']?>, se ha identificado como <?php echo $_SESSION['rol'] ?></h1>
	<form action="" method="POST">
		<fieldset>
			<legend>Menú de opciones</legend>
			<p><button type="submit" name="vercitas">Ver todas las citas</button></p>
			<p><button type="submit" name="cerrarsesion">Cerrar Sesión</button></p>
		</fieldset>
	</form>	

	<?php

			if (isset($_POST['vercitas'])) {

				header("Location:verCitas.php");

			}

			if (isset($_POST['cerrarsesion'])) {

					session_destroy();
			 
					header("Location:index.php");
			}

		}
	}
	mysqli_close($conexion);
	?>
</body>
</html>