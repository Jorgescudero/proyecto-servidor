<!DOCTYPE html>
<html lang="es">
<head>
	<title>Centro Médico Virtual</title>
	<meta charset="utf-8">
	<style type="text/css">
		body{
			background-color: lightgrey;
			text-align: center;
		}
		form{
			background-color: white;
			text-align: center;
			width: 400px;
			margin-left: 32%;
		}
		input{
			border-color: darkgrey;
		}
		button{
			width: 250px;
			background-color: darkgrey;
			color: white;
			border-color: black;
		}
		table{
			background-color: white;
			margin-left: 27%;
			width: 500px;
		}
	</style>
</head>
<body>
	<h1>Centro médico</h1>
	<form action="" method="POST">
		<fieldset>
			<legend>Inicio de sesión</legend>
			<p>Usuario: <input type="text" name="user" required="required"></p>
			<p>Contraseña: <input type="password" name="pass" required="required"></p>
			<p><input type="submit" name="enviar" value="Entrar"></p>
		</fieldset>
	</form>

	<?php

	$conexion = mysqli_connect('localhost', 'usuacceso', 'usuacceso', 'consultas');
	if (mysqli_connect_errno()) {
	    printf("Conexión fallida %s\n", mysqli_connect_error());
	    exit();
	}

	if (isset($_POST['enviar'])) {
		$usuario=$_POST['user'];
		$password=$_POST['pass'];
		$sql = "SELECT dniUsu,usutipo FROM usuarios WHERE usuLogin = '$usuario' AND usuPassword = '$password'";
		$result = mysqli_query ($conexion, $sql);

		if(mysqli_num_rows($result) > 0) {
			while ($registro = mysqli_fetch_row($result)) {
				$nif=$registro[0];
                $rol=$registro[1];
        	}

	        session_start();

	     	$_SESSION['nif']="$nif";
	        $_SESSION['rol']="$rol";
	        $_SESSION['user']=$usuario;

	        header("Location: inicio.php");
     
        	exit();
		}
		else {
			$mensajeaccesoincorrecto = "<p>El usuario o contraseña no son correctos, vuelva a introducirlos</p>";
        	 echo $mensajeaccesoincorrecto;
		}
	}
	mysqli_close($conexion);

	?>
</body>
</html>