<?php

session_start();

$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu4'], $_SESSION['pass4'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Citas</title>
	<meta charset="utf-8">
	<style type="text/css">
	body{
			background-color: lightgrey;
			text-align: center;
		}
	form{
		background-color: white;
		width: 400px;
		margin-left: 34%;
	}
	button{
			width: 250px;
			background-color: darkgrey;
			color: white;
			border-color: black;
	}
	table{
			background-color: white;
			width: 500px;
			margin-left: 29%;
		}
	</style>
</head>
<body>
	<h1>Bienvenido/a, se ha identificado como <?php echo $_SESSION['rol'] ?></h1>
	<div>
		<form action="" method="POST">
			<button type="submit" name="back">Volver al menú</button>
			<button type="submit" name="cerrarsesion">Cerrar Sesión</button>
		</form>
	</div>
	<h3>Listado de citas</h3>
	<table border="1" style="text-align: center;">
		<tr>
			<th>Fecha</th>
			<th>Hora</th>
			<th>Médico</th>
			<th>Consultorio</th>
			<th>Estado</th>
			<th>Observaciones</th>
		</tr>

		<?php

		$nif=$_SESSION['nif'];


		$sql="SELECT citas.citFecha,citas.citHora,medicos.medNombres,medicos.medApellidos,consultorios.conNombre,citas.citEstado,citas.CitObservaciones FROM citas,medicos,consultorios WHERE citas.citPaciente='$nif' AND citas.citMedico=medicos.dniMed AND citas.citConsultorio=consultorios.idConsultorio;";
		$result = mysqli_query ($conexion, $sql);
		$filas=mysqli_num_rows($result);
		if ($filas>0) {
			while ($registro = mysqli_fetch_row($result)) {
				
		?>

		<tr>
			<td><?php echo $registro[0]; ?></td>
			<td><?php echo $registro[1]; ?></td>
			<td><?php echo $registro[2]." ".$registro[3]; ?></td>
			<td><?php echo $registro[4]; ?></td>
			<td><?php echo $registro[5]; ?></td>
			<td><?php echo $registro[6]; ?></td>
		</tr>

		<?php

			}
		}
		else {
			echo "<tr><td colspan='6'>No tiene ninguna cita</td></tr>";
		}

		?>

	</table>

	<?php

	if (isset($_POST['back'])) {

		header("Location:inicio.php");

	}

	if (isset($_POST['cerrarsesion'])) {

		session_destroy();
			 
		header("Location:index.php");
	}
	
	mysqli_close($conexion);

	?>
</body>
</html>