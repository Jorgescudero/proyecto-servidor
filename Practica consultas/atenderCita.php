<?php

session_start();

$conexion=mysqli_connect($_SESSION['servidor'], $_SESSION['usu2'], $_SESSION['pass2'], $_SESSION['basedatos']);
			if (mysqli_connect_errno()) {
	    		printf("Conexión fallida %s\n", mysqli_connect_error());
	    		exit();
			}

$nif=$_SESSION['atenderpaciente'][0];
$fecha=$_SESSION['atenderpaciente'][1];
$hora=$_SESSION['atenderpaciente'][2];	

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Atender cita</title>
	<meta charset="utf-8">
	<style type="text/css">
		body{
			background-color: lightgrey;
			text-align: center;
		}
		.botones{
			margin-left: 90px;
		}
		form{
			background-color: lightgrey;
			text-align: center;
			width: 400px;
			margin-left: 27%;
		}
		input{
			border-color: darkgrey;
		}
		button{
			width: 250px;
			background-color: darkgrey;
			color: white;
			border-color: black;
		}
		table{
			background-color: white;
			width: 500px;
		}
		fieldset{
			background-color: white;
			border-color: black;
		}
	</style>
</head>
<body>
	<h1>Bienvenido/a, se ha identificado como <?php echo $_SESSION['rol'] ?></h1>
	<div class="botones">
		<form action="" method="POST">
			<button type="submit" name="back">Volver al menú anterior</button>
			<button type="submit" name="cerrarsesion">Cerrar Sesión</button>
		</form>
	</div>
	<form action="" method="POST">
		<fieldset>
			<legend>Atender cita</legend>
			<table border="1" style="text-align: center;">
				<tr>
					<th>DNI paciente</th>
					<td><?php echo $nif; ?></td>
				</tr>
				<tr>
					<th>Nombre paciente</th>
					<?php

					$sql="SELECT pacNombres,pacApellidos FROM pacientes WHERE dniPac='$nif';";
					$result = mysqli_query ($conexion, $sql);
					$registro=mysqli_fetch_row($result);


					?>
					<td><?php echo $registro[0]." ".$registro[1]; ?></td>
				</tr>
				<tr>
					<th>Fecha cita</th>
					<td><?php echo $fecha; ?></td>
				</tr>
				<tr>
					<th>Hora cita</th>
					<td><?php echo $hora; ?></td>
				</tr>
				<tr>
					<th>Observaciones</th>
					<td><textarea name="obs" placeholder="Escriba aquí las observaciones del paciente" style="box-sizing: border-box; width: 350px; height: 200px; resize: none; overflow: auto;" required="required"></textarea></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" name="atender" value="Enviar"></td>
				</tr>
			</table>
		</fieldset>
	</form>

	<?php

	if (isset($_POST['atender'])) {
		$observaciones=$_POST['obs'];
		$sql="UPDATE citas SET citEstado='Atendido', CitObservaciones='$observaciones' WHERE citPaciente='$nif' AND citFecha='$fecha' AND citHora='$hora';";
		if (mysqli_query($conexion, $sql)) {
			 	echo "<p> Se han registrado las observaciones con éxito</p>";
			}
		else {
			echo " <br> Error: " . $sql . "<br>" . mysqli_error($conexion);
		}
	}

	if (isset($_POST['back'])) {

		header("Location:citasPendientes.php");

	}

	if (isset($_POST['cerrarsesion'])) {

		session_destroy();
			 
		header("Location:Proyectofinal1EVA.php");
	}

	mysqli_close($conexion);

	?>
</body>
</html>