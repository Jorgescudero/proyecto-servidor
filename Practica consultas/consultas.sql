-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-01-2020 a las 11:57:27
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `consultas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `citas`
--

CREATE TABLE `citas` (
  `idCita` int(11) NOT NULL,
  `citFecha` date NOT NULL,
  `citHora` time NOT NULL,
  `citPaciente` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `citMedico` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `citConsultorio` int(11) NOT NULL,
  `citEstado` enum('Asignado','Atendido') COLLATE utf8_spanish_ci NOT NULL DEFAULT 'Asignado',
  `CitObservaciones` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `citas`
--

INSERT INTO `citas` (`idCita`, `citFecha`, `citHora`, `citPaciente`, `citMedico`, `citConsultorio`, `citEstado`, `CitObservaciones`) VALUES
(1, '2020-01-23', '10:30:00', '71677267K', '71677266L', 6, 'Atendido', 'Se va a morir'),
(2, '2020-01-17', '11:25:00', '78596324L', '12345678K', 4, 'Asignado', ''),
(3, '2020-01-15', '14:20:00', '78596324L', '71677266L', 5, 'Asignado', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consultorios`
--

CREATE TABLE `consultorios` (
  `idConsultorio` int(11) NOT NULL,
  `conNombre` char(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `consultorios`
--

INSERT INTO `consultorios` (`idConsultorio`, `conNombre`) VALUES
(1, 'Centro de Salud Oviedo'),
(2, 'Centro de Salud Corvera'),
(3, 'Centro de Salud Aviles'),
(4, 'Centro de Salud Gijon'),
(5, 'Centro de Salud Luarca'),
(6, 'Hospital Universitario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medicos`
--

CREATE TABLE `medicos` (
  `dniMed` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `medNombres` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `medApellidos` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `medEspecialidad` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `medTelefono` char(15) COLLATE utf8_spanish_ci NOT NULL,
  `medCorreo` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `medicos`
--

INSERT INTO `medicos` (`dniMed`, `medNombres`, `medApellidos`, `medEspecialidad`, `medTelefono`, `medCorreo`) VALUES
('12345678K', 'David', 'Rodriguez', 'enfermeria', '985862471', 'david@gmail.com'),
('71677266L', 'Eva', 'Hernandez', 'Quiropractica', '669696696', 'eva@gmail.com'),
('71677268P', 'Bea', 'Sanchez', 'matrona', '675822695', 'bea@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pacientes`
--

CREATE TABLE `pacientes` (
  `dniPac` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `pacNombres` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `pacApellidos` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `pacFechaNacimiento` date NOT NULL,
  `pacSexo` enum('Masculino','Femenino') COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `pacientes`
--

INSERT INTO `pacientes` (`dniPac`, `pacNombres`, `pacApellidos`, `pacFechaNacimiento`, `pacSexo`) VALUES
('71677267K', 'Jorge', 'Escudero', '1998-10-17', 'Masculino'),
('78562475M', 'Alex', 'Suarez', '2003-02-07', 'Masculino'),
('78596324L', 'Javi', 'Barroso', '1992-07-06', 'Masculino');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `dniUsu` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `usuLogin` char(15) COLLATE utf8_spanish_ci NOT NULL,
  `usuPassword` varchar(157) COLLATE utf8_spanish_ci NOT NULL,
  `usuEstado` enum('Activo','Inactivo') COLLATE utf8_spanish_ci NOT NULL,
  `usutipo` enum('Administrador','Asistente','Medico','Paciente') COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`dniUsu`, `usuLogin`, `usuPassword`, `usuEstado`, `usutipo`) VALUES
('11111a', 'pepe', 'pepe1', 'Activo', 'Administrador'),
('12345678K', 'david', 'david1', 'Activo', 'Medico'),
('22222b', 'ana', 'ana1', 'Activo', 'Paciente'),
('33333c', 'javi', 'javi1', 'Activo', 'Asistente'),
('44444d', 'hector', 'hector1', 'Activo', 'Medico'),
('71677266L', 'eva', 'eva1', 'Activo', 'Medico'),
('71677267K', 'jorge', 'jorge1', 'Activo', 'Paciente'),
('71677268P', 'bea', 'bea1', 'Activo', 'Medico'),
('78562475M', 'alex', 'alex1', 'Activo', 'Paciente');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `citas`
--
ALTER TABLE `citas`
  ADD PRIMARY KEY (`idCita`),
  ADD KEY `citPaciente` (`citPaciente`,`citMedico`,`citConsultorio`),
  ADD KEY `citMedico` (`citMedico`),
  ADD KEY `citConsultorio` (`citConsultorio`);

--
-- Indices de la tabla `consultorios`
--
ALTER TABLE `consultorios`
  ADD PRIMARY KEY (`idConsultorio`);

--
-- Indices de la tabla `medicos`
--
ALTER TABLE `medicos`
  ADD PRIMARY KEY (`dniMed`);

--
-- Indices de la tabla `pacientes`
--
ALTER TABLE `pacientes`
  ADD PRIMARY KEY (`dniPac`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`dniUsu`,`usuLogin`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `citas`
--
ALTER TABLE `citas`
  MODIFY `idCita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `citas`
--
ALTER TABLE `citas`
  ADD CONSTRAINT `citas_ibfk_1` FOREIGN KEY (`citPaciente`) REFERENCES `pacientes` (`dniPac`),
  ADD CONSTRAINT `citas_ibfk_2` FOREIGN KEY (`citMedico`) REFERENCES `medicos` (`dniMed`),
  ADD CONSTRAINT `citas_ibfk_3` FOREIGN KEY (`citConsultorio`) REFERENCES `consultorios` (`idConsultorio`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

GRANT USAGE ON *.* TO 'administrador'@'localhost' IDENTIFIED BY PASSWORD '*EFC258DA67C9F942793CDD8A2050C469256C7192';

GRANT ALL PRIVILEGES ON `consultas`.`consultorios` TO 'administrador'@'localhost' WITH GRANT OPTION;

GRANT ALL PRIVILEGES ON `consultas`.`citas` TO 'administrador'@'localhost' WITH GRANT OPTION;

GRANT ALL PRIVILEGES ON `consultas`.`pacientes` TO 'administrador'@'localhost' WITH GRANT OPTION;

GRANT ALL PRIVILEGES ON `consultas`.`usuarios` TO 'administrador'@'localhost' WITH GRANT OPTION;

GRANT ALL PRIVILEGES ON `consultas`.`medicos` TO 'administrador'@'localhost' WITH GRANT OPTION;

/*///////////////////////////////////////////*/

GRANT USAGE ON *.* TO 'asistente'@'localhost' IDENTIFIED BY PASSWORD '*4963DD1E44AE96E982437A04E131AA894BDBADFE';

GRANT SELECT, INSERT, UPDATE, REFERENCES ON `consultas`.`medicos` TO 'asistente'@'localhost';

GRANT SELECT ON `consultas`.`consultorios` TO 'asistente'@'localhost';

GRANT SELECT, INSERT ON `consultas`.`pacientes` TO 'asistente'@'localhost';

GRANT SELECT, INSERT ON `consultas`.`citas` TO 'asistente'@'localhost';

GRANT INSERT ON `consultas`.`usuarios` TO 'asistente'@'localhost';

/*///////////////////////////////////////////*/

GRANT USAGE ON *.* TO 'medico'@'localhost' IDENTIFIED BY PASSWORD '*98D53BF578FEB432F045DF1D3060845370BFC537';

GRANT SELECT, INSERT, UPDATE, REFERENCES ON `consultas`.`medicos` TO 'medico'@'localhost';

GRANT SELECT, INSERT, UPDATE ON `consultas`.`citas` TO 'medico'@'localhost';

GRANT SELECT ON `consultas`.`pacientes` TO 'medico'@'localhost';

GRANT SELECT ON `consultas`.`consultorios` TO 'medico'@'localhost';

/*///////////////////////////////////////////*/

GRANT USAGE ON *.* TO 'paciente'@'localhost' IDENTIFIED BY PASSWORD '*14F57843D3EDA0C1BAAE3AAA7C6CF4B627ECB763';

GRANT SELECT ON `consultas`.`medicos` TO 'paciente'@'localhost';

GRANT SELECT ON `consultas`.`pacientes` TO 'paciente'@'localhost';

GRANT SELECT ON `consultas`.`citas` TO 'paciente'@'localhost';

GRANT SELECT ON `consultas`.`consultorios` TO 'paciente'@'localhost';

/*///////////////////////////////////////////*/

GRANT USAGE ON *.* TO 'usuacceso'@'localhost' IDENTIFIED BY PASSWORD '*6C62C7F187387CD3EED4ABCB3A3F6309AB3E19DD';

GRANT SELECT (usutipo, usuLogin, dniUsu, usuPassword) ON `consultas`.`usuarios` TO 'usuacceso'@'localhost';