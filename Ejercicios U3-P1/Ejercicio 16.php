<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Ejercicio 16</title>
</head>
<body>
    <?php
        $numero1=rand ( 1 , 10);
        $numero2=rand ( 1 , 10);
        if($numero1>$numero2){
            $mayor=$numero1;
        }
        else{
            $mayor=$numero2;
        }
        if($numero1%2==0){
            $par1="par";
        }
        else{
            $par1="impar";
        }
        if($numero2%2==0){
            $par2="par";
        }
        else{
            $par2="impar";
        }
        echo("Numeros: $numero1-$numero2</br>Mayor: $mayor</br>$numero1 es $par1</br>$numero2 es $par2");
        ?>
</body>
</html>