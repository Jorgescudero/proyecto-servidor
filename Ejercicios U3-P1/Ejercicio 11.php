<!DOCTYPE html>
<html>
<head>
	<title>Ejercicio 11</title>
</head>
<body>
	<?php
		$variable = "Cielo";
		// El Cielo es azul
		echo "El $variable es azul";
		// El $variable es azul
		echo 'El $variable es azul';

		/*Lo que pasa es que con las dobles comillas se interpreta bien la variable, y con las comillas simples escribe de manera literal lo que se escribe*/
	?>


</body>
</html>