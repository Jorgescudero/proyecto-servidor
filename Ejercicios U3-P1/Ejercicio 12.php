<!DOCTYPE html>
<html>
<head>
	<title>Ejercicio 12</title>
</head>
<body>

	<?php
	//echo vs print. Ambos son constructores de lenguaje que muestran cadenas de texto con diferencias algo sutiles. Esta es la forma y tipo de cada uno:

	int print ( string $arg );
	void echo ( string $arg1 [, string $... ] )

	//print imprime una cadena, echo puede imprimir más de una separadas por coma:

	print 'Hola';
	echo 'Hola', 'Hola de nuevo';

	//print devuelve un valor int que según la documentación siempre es 1, por lo que puede ser utilizado en expresiones mientras que echo es tipo void, no hay valor devuelto y no puede ser utilizado en expresiones:

	//Se imprime "Hola" y la variable $foo toma el valor 1
	$foo = print 'Hola';
	//La siguiente expresión da error
	$foo = echo 'Hola';





	?>

</body>
</html>