<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Ejercicio 27</title>
</head>
<body>
   <?php
    echo "Numeros primos del 1 al 50 <br>";
    for ($i=1; $i<=50; $i++) {
        $div = 2;
        $primo = true;
        do {
            if ($i % $div == 0)
            $primo = false;
            $div++;
        } while(($i >= $div*2) && ($primo == TRUE));
        if ($primo == TRUE )
             echo $i.", ";
    }
    ?>
</body>
</html>