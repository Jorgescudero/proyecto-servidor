<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Ejercicio 18</title>
</head>
<body>
    <?php
        echo("La función isset(), según el manual PHP, determina si una variable ha sido declarada y su valor no es NULO.</br>");
        echo("La función empty(), determina si una variable está vacía.</br>");
        echo("El método GET envía la información codificada del usuario en el header del HTTP request, directamente en la URL.</br>");
        echo("Con el método HTTP POST también se codifica la información, pero ésta se envía a través del body del HTTP Request, por lo que no aparece en la URL.");
    ?>
</body>
</html>