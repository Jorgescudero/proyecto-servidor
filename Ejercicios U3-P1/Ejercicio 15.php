<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Ejercicio 15</title>
</head>
<body>
    <?php
        $salario=10;
        $horas=60;
        if($horas<=40){
            $paga=$salario*$horas;
        }
        else if($horas>40 && $horas<=48){
            $paga=$salario*40+(($horas-40)*($salario*2));
        }
        else{
            $paga=($salario*40+(8*($salario*2)))+(($horas-48)*$salario*3);
        }
        echo("El trabajador gana $paga €");
    ?>
</body>
</html>