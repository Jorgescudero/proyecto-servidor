<!DOCTYPE html>
<html>
<head>
	<title>Ejercicio 34</title>
</head>
<style type="text/css">
	.Cal{
		background-color: blue;
		color:white;
	}
	.ds{
		background-color: black;
		color:white;
	}
	table{
		background-color: red;
	}
	.dias{
		background-color: #F7FF4B;
	}
</style>
<body>
	
	<h3>Calendario</h3><br>
	<form action="" method="POST">
		Mes:<input type="number" name="mes"/> <br>
		Ano:<input type="number" name="ano"/> <br>
		<br>
		<input type="submit" name="enviar" value="Enviar"/>
	</form>
	<?php

	if(isset($_POST['enviar'])){
		$mes = $_POST['mes'];
		$ano = $_POST['ano'];
		$dia = JDDayOfWeek(cal_to_jd(CAL_GREGORIAN, date($mes), date(1), date($ano)),0);
		if ($dia==0) {
			$dia=7;
		}
		$ndias = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);
		$meses = array(1=>"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");	
	
	?>

	<br><br>

	<table border="5">
		<tr>
			<th colspan="7" class="Cal">Calendario <?php echo $meses[$mes]." ".$ano;?>
		</tr>
		<tr>
			<th class="ds">Lunes</th><th class="ds">Martes</th><th class="ds">Miercoles</th><th class="ds">Jueves</th><th class="ds">Viernes</th><th class="ds">Sabado</th><th class="ds">Domingo</th>
		</tr>
		<?php
		$inicio=true;
		$n=1;
		do{
			echo "<tr aling='center'>";
			if($inicio){
				for($i=1;$i<=7;$i++){
					while ($i< $dia && $i<=7) {
						echo "<td></td>";
						$i++;
					}
					echo '<td class="dias">'.$n."</td>";
					$n++;
				}
				$inicio=false;
			}else{
				for($i=1;$i<=7;$i++){
					echo '<td class="dias">'.$n."</td>";
					$n++;
					if($n>$ndias){
						for ($k=0; $k <=(7-$i) ; $k++) { 
							echo "<td></td>";
						}
						break;
					}
				}
				echo "</tr>";
			}

		}while($n<=$ndias);
}

		?>
	</table>
	
</body>
</html>