<!DOCTYPE html>
<html>
<head>
	<title>Ejercicio 2</title>
</head>
<style type="text/css">
  body{
    background-color: lightgrey;
    padding-left: 75px;
  }
  div{
    position: relative;
    float: left;
    margin-right: 20px;
    border: solid #3A3A3A;
    width: 200px;
    height: 200px;
    margin-top: 20px;

  }
  img{
    width: 150px;
    height: 120px;
    margin-left: 20px;
    margin-right: 20px;
    margin-top: 5px;
  }
  p{
    text-align: center;
    background-color: #3A3A3A;
    color: white;
  }

  .final{
    border:none;
    margin-top: 20px;
  }
</style>
<body>
  <h3>Felicidades</h3>
  <h4>Usted acaba de adquirir:</h4>
	<?php 
	session_start();
  $preciototal=0;
  foreach ($_SESSION["productos"] as $key => $cosa) {
      if ($cosa>0) {
        echo "<div><img src=".$_SESSION["imagenes"][$key]."></img>";;
        $precio=($cosa*$_SESSION["precios"][$key]);
      echo "<p>Producto:".$key ." <br>Unidades " .$cosa ." <br>Total:".$precio ."€ </p></div>";
      $preciototal+=$precio;
      }
    }

    echo "<div class='final'>Precio a pagar: " .$preciototal ."€";
    ?>
    <h4>Gracias por su compra</h4>
    <br>
    <strong><a href="formularioguapo.php" onclick="<?php session_destory()?>">Terminar</a></strong>
  </div>
</body>
</html>