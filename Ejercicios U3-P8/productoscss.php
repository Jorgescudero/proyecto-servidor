<!DOCTYPE html>
<html>
<head>
	<title>Ejercicio 2</title>
</head>
<style type="text/css">
  img{
    width: 150px;
    height: 120px;
  }
  body{
    background-color: lightgrey;
  }
  .tabla{
    background-color: white;
    margin-left: 12%;
    width: 500px;
  }
  .table1{
    border:solid #3A3A3A;
    background-color: #2A2A2A;
  }
  .saludo{
    color: #3A3A3A;
    margin-left: 36%;
    background-color: white;
    width: 325px;
    padding-left: 40px;

  }
  h3{
    color: #3A3A3A;
    background-color: white;
    width: 400px;
  }
  .titulo{
    color: #3A3A3A;
    text-align: center;
  }
  .td1{
    border:solid #3A3A3A;
    margin: auto;
    background-color: white;
    text-align: center;
  }
  .carrito{
    margin-left: 60%;
    margin-top: -500px;
    position: relative;
    background-color: #3A3A3A;
    border:solid white;
    width: 400px;
    border: solid #3A3A3A;

  }
  .table2{
    border:solid white;
    background-color: #3A3A3A;
    color: white;
  }
  .td2{
    border:solid #3A3A3A;
    margin: auto;
    text-align: center;
  }
  th{
    border:solid #3A3A3A;
    border-top: none;
  }
  .confirmar{
    margin-left: 40px;
    background-color: white;
    color: #3A3A3A;
    margin-bottom: 5px;
  }
  header{
    width: 100%;
    background-color: white;
    color :#3A3A3A;
    text-align: center;
  }
  h1{
    margin-top: 30px;
    margin-bottom: 50px;
  } 
  .agregar{
    position: center;
    color: white;
    background-color: #3A3A3A;
    padding-bottom: 0%;
  }

</style>
<body>
  <header>
    <h1>TECNOMUNDO</h1>
  </header>
	<?php 
	session_start();
  if (isset($_POST["Ingresar"])) {
    $nombre = $_POST["usuario"];
    $_SESSION["usuario"]=$nombre;
    ?>
    <script type="text/javascript">
    alert("Bienvenido a TecnoMundo, <?php echo($_SESSION['usuario'])?>");
  </script>
  <?php
    
    
    $carrito = array('televisor' => 0,'movil' => 0,'ps4' => 0,'raton' => 0,'alfombrilla' => 0,'usb' => 0 );
    $precio = array('televisor' => 210,'movil' => 1100,'ps4' => 300,'raton' => 20,'alfombrilla' => 30,'usb' => 20 );
    $imagenes = array ('televisor' => "televisor.jpg",'movil' => "movil.jpg",'ps4' => "ps4.jpg",'raton' => "raton.jpg",'alfombrilla' => "alfombrilla.jpg",'usb' => "usb.jpg" );
    $_SESSION["productos"] = $carrito;
    $_SESSION["precios"] = $precio;
    $_SESSION["imagenes"] = $imagenes;
  }
  $imagenes = array ('televisor' => "televisor.jpg",'movil' => "movil.jpg",'ps4' => "ps4.jpg",'raton' => "raton.jpg",'alfombrilla' => "alfombrilla.jpg",'usb' => "usb.jpg" );
   $_SESSION["imagenes"] = $imagenes;
  echo ("<h3 class='saludo'>Bienvenid@ a TecnoMundo, ".$_SESSION["usuario"]."</h3>");
	$televisor = array("producto" => "Televisor 4K","descripcion" => "22 pulgadas","precio" => 210,);
	$movil = array("producto" => "Iphone 11","descripcion" => "4g","precio" => 1100);
	$ps4 = array("producto" => "ps4","descripcion" => "1T de memoria","precio" => 250);
	$raton = array("producto" => "Raton Gaming","descripcion" => "6000 dpi","precio" => 20);
	$alfombrilla = array("producto" => "Alfombrilla Gaming","descripcion" => "negra","precio" => 30);
	$usb = array("producto" => "usb","descripcion" => "1T","precio" => 20);
 
	?>
	<br>
	<br>
	<?php
	$lista = array("televisor"=>$televisor,"movil"=>$movil,"ps4"=>$ps4, "raton"=>$raton,"alfombrilla"=>$alfombrilla,"usb"=>$usb);
  ?>
<div class="tabla">
  <form action="controlcss.php" method="POST">
    <table class="table1">
      <tr>
        <?php
        $i = 1;
        foreach ($lista as $key => $value) {
          echo "<td class='td1'>";
          echo "<img src=".$_SESSION["imagenes"][$key]."></img>";
          foreach ($value as $campo => $dato) { ?>
            <p> <?php echo $campo ?>: <b><?php echo $dato;}?>
              </b></p>
            <button class="agregar" type="submit" name="agregar" value=<?php echo $key;?>>agregar</button>
            </td>
          <?php
            $i++;
            if ($i % 4 == 0) {
              echo "</tr><tr>";
            }
          }
          ?>
      </tr>
    </table>
  </form>
</div>
<?php
$preciototal=0;
 foreach ($_SESSION["productos"] as $key => $cosa) {
      if ($cosa>0) {
        $precio=($cosa*$_SESSION["precios"][$key]);
        $preciototal+=$precio;
      }
    }
?>
  <br><br><br>
  <?php if (($_SESSION["productos"]["televisor"]>0) || ($_SESSION["productos"]["movil"]>0) || ($_SESSION["productos"]["ps4"]>0) || ($_SESSION["productos"]["raton"]>0) || ($_SESSION["productos"]["usb"]>0) || ($_SESSION["productos"]["alfombrilla"]>0)) {
    ?>
  
  <div class="carrito">
  <h3 class="titulo">CARRITO COMPRA</h3>
  <ul>
    <?php
    echo "<form action='controlcss.php' method=POST>";
    echo "<table class='table2'>";
    echo "<tr><th>Producto</th><th>Unidades</th><th>Precio</th><th>Rechazar</th></tr>";
    foreach ($_SESSION["productos"] as $key => $cosa) {
      if ($cosa>0) {
        $precio=($cosa*$_SESSION["precios"][$key]);
      echo "<tr>";
      echo "<td>".$key ."</td><td> " .$cosa ."</td><td> ".$precio ."€</td>";
      echo "<td><button type='submit' name='quitar' value='$key'>Quitar</td>";
      echo "</tr>";
      }
    }
    echo "<td>Total: </td><td>".$preciototal."€</td></tr>";

    echo "</form></table>";

    ?>
  </ul>
  <form action="confirmacionguapo.php" method="POST">
  	<button  class='confirmar' type="submit" name="confirmar" value="Confirmar compra">Confirmar compra</button>
  </form>
</div>
<?php } ?>
</body>
</html>